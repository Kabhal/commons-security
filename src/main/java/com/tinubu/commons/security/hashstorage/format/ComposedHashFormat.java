/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.format;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.stream.Collectors.joining;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.security.hashstorage.HashStorage;
import com.tinubu.commons.security.hashstorage.algorithm.HashAlgorithm;
import com.tinubu.commons.security.hashstorage.algorithm.HashAlgorithmProvider;
import com.tinubu.commons.security.hashstorage.algorithm.HashAlgorithmRegistry;
import com.tinubu.commons.security.hashstorage.algorithm.Salt;
import com.tinubu.commons.security.hashstorage.crypto.Base64;
import com.tinubu.commons.security.hashstorage.strategy.ComposedHashStrategy;

/**
 * Custom hash format representation supporting composed hash algorithms with metadata.
 * <p>
 * The format representation is basically :
 * <p><em>{@code {<algorithm>}<hash>}</em></p>
 * <ul>
 *    <li>{@code algorithm} : Algorithm name (supported characters : [a-zA-Z0-9-], but you should keep at [a-z0-9])</li>
 *    <li>{@code hash} : Hash data encoded in Base64 (without padding at formatting, padding is supported at parsing)</li>
 * </ul>
 * <p>
 * The format support an arbitrary list of parameters :
 * <p><em>{@code {<algorithm>;<key>=<value>;<key>=<value>;...}<hash>}</em></p>
 * <ul>
 *    <li>{@code key} : Parameter name (supported characters : [a-zA-Z0-9-], but you should keep at [a-z0-9])</li>
 *    <li>{@code value} : Parameter value (supported characters : any character not in [{};], '=' is supported in value)</li>
 * </ul>
 * Note : you can use Base64 encoded parameter values if you need an extended set of supported characters for a given parameter.
 * <p>
 *  The format supports multiple composed hash algorithms :
 *  <p><em>{@code {<algorithm1>;<key>=<value>}{<algorithm2>;<key>=<value>}<hash>}</em></p>
 *  The number of composed algorithms is not limited, the final hash function applied to payload is {@code algorithm1(algorithm2(payload))}.
 */
public class ComposedHashFormat implements HashFormat {

   /**
    * Strategy's parameters separator.
    */
   private static final char PARAMETERS_SEPARATOR = ';';
   /**
    * Key value of parameters separator.
    */
   private static final char PARAMETER_VALUE_SEPARATOR = '=';
   /**
    * Salt parameter name.
    */
   public static final String SALT_PARAMETER = "salt";
   /** Regexp matching the complete format to identify specifications and hash. */
   private static final Pattern FORMAT_PATTERN = Pattern.compile("((?:\\{[^}]+})+)(.+)");
   /** Regexp matching a single specification to identify hash algorithm and its parameters. */
   private static final Pattern SPECIFICATION_PATTERN = Pattern.compile(String.format(
         "\\{([a-zA-Z0-9-]+)((?:%1$c[a-zA-Z0-9-]+%2$c[^{}%1$c]+)*)}",
         PARAMETERS_SEPARATOR,
         PARAMETER_VALUE_SEPARATOR));

   @Override
   public boolean supportMultipleHashAlgorithms() {
      return true;
   }

   @Override
   public String format(HashStorage hashStorage) {
      notNull(hashStorage, "hashStorage");

      List<HashAlgorithm> composedHashAlgorithms = new ArrayList<>(hashStorage.hashAlgorithms());
      Collections.reverse(composedHashAlgorithms);

      String algorithmSpecifications = composedHashAlgorithms
            .stream()
            .map(ComposedHashFormat::algorithmSpecification)
            .collect(joining(""));
      String hashBase64 = Base64.encodeToString(hashStorage.hash(), false);

      return algorithmSpecifications + hashBase64;
   }

   @Override
   public HashStorage parse(String hashFormat) {
      notBlank(hashFormat, "hashFormat");

      Matcher formatMatcher = FORMAT_PATTERN.matcher(hashFormat);

      if (formatMatcher.matches()) {

         try {
            String specificationPart = formatMatcher.group(1);
            String hashPart = formatMatcher.group(2);
            byte[] hash = Base64.decode(hashPart.getBytes(StandardCharsets.UTF_8));

            Matcher matcher = SPECIFICATION_PATTERN.matcher(specificationPart);

            ComposedHashStrategy strategy = null;
            while (matcher.find()) {
               String hashAlgorithmName = matcher.group(1);
               Map<String, String> hashParameters = parseParametersSpecification(matcher.group(2));
               Salt salt = saltParameter(hashParameters);
               HashAlgorithm hashAlgorithm =
                     hashAlgorithmProvider(hashAlgorithmName).fromFormat(salt, hashParameters);

               strategy = composeHashAlgorithm(strategy, hashAlgorithm);
            }

            if (strategy == null) {
               throw new IllegalArgumentException("No specification part");
            }

            return HashStorage.ofHash(strategy, hash);
         } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(String.format("Bad format : %s", hashFormat), e);
         }
      } else {
         throw new IllegalArgumentException(String.format("Bad format : %s", hashFormat));
      }
   }

   private static Salt saltParameter(Map<String, String> hashParameters) {
      return nullable(hashParameters.get(SALT_PARAMETER))
            .map(s -> Base64.decode(s.getBytes(StandardCharsets.UTF_8)))
            .map(Salt::ofBytes)
            .orElse(null);
   }

   private static HashAlgorithmProvider hashAlgorithmProvider(String hashAlgorithmName) {
      return HashAlgorithmRegistry
            .instance()
            .hashAlgorithmProvider(hashAlgorithmName)
            .orElseThrow(() -> new IllegalStateException(String.format("Unknown '%s' hash algorithm",
                                                                       hashAlgorithmName)));
   }

   private static ComposedHashStrategy composeHashAlgorithm(ComposedHashStrategy hashStrategy,
                                                            HashAlgorithm hashAlgorithm) {
      if (hashStrategy == null) {
         return new ComposedHashStrategy(hashAlgorithm);
      } else {
         return hashStrategy.compose(hashAlgorithm);
      }
   }

   private static String algorithmSpecification(HashAlgorithm hashAlgorithm) {
      Set<Entry<String, String>> parameters = new LinkedHashSet<>();

      hashAlgorithm
            .salt()
            .ifPresent(salt -> parameters.add(Pair.of(SALT_PARAMETER,
                                                      Base64.encodeToString(salt.asBytes(), false))));
      if (hashAlgorithm.parameters() != null) {
         parameters.addAll(hashAlgorithm.parameters().entrySet());
      }

      String parametersSpecification = parameters
            .stream()
            .filter(e -> e.getKey() != null && e.getValue() != null)
            .map(e -> e.getKey() + PARAMETER_VALUE_SEPARATOR + e.getValue())
            .collect(joining(String.valueOf(PARAMETERS_SEPARATOR)));
      return String.format("{%s%s%s}",
                           normalizeHashAlgorithmName(hashAlgorithm.algorithmName()),
                           parametersSpecification.trim().isEmpty()
                           ? ""
                           : String.valueOf(PARAMETERS_SEPARATOR),
                           parametersSpecification);
   }

   private static String normalizeHashAlgorithmName(String algorithmName) {
      return algorithmName.toLowerCase().replaceAll("[^a-z0-9]+", "");
   }

   private static Map<String, String> parseParametersSpecification(String parametersSpecification) {
      Map<String, String> parameters = new LinkedHashMap<>();

      if (!parametersSpecification.trim().isEmpty()) {
         String[] parameterGroups =
               parametersSpecification.substring(1).split(String.valueOf(PARAMETERS_SEPARATOR));

         Arrays.stream(parameterGroups).forEach(p -> {
            String[] parameter = p.split(String.valueOf(PARAMETER_VALUE_SEPARATOR), 2);
            parameters.put(parameter[0], parameter[1]);
         });
      }

      return parameters;
   }
}
