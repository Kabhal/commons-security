/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.crypto;

import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class Random {

   private static final SecureRandom DEFAULT_RANDOM_GENERATOR = randomGenerator("SHA1PRNG");

   private final SecureRandom randomGenerator;

   public Random(SecureRandom randomGenerator) {
      this.randomGenerator = notNull(randomGenerator, "randomGenerator");
   }

   public Random(String providerName) {
      this(randomGenerator(notBlank(providerName, "providerName")));
   }

   public Random() {
      this(DEFAULT_RANDOM_GENERATOR);
   }

   public byte[] random(int length) {
      byte[] salt = new byte[length];

      randomGenerator.nextBytes(salt);

      return salt;
   }

   private static SecureRandom randomGenerator(String provider) {
      try {
         return SecureRandom.getInstance(provider);
      } catch (NoSuchAlgorithmException e) {
         throw new IllegalArgumentException(String.format("Unknown random generator provider '%s'", provider),
                                            e);
      }
   }

}
