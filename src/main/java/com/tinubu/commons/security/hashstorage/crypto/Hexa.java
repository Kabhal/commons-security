/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.crypto;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

public final class Hexa {

   private Hexa() {}

   public static char[] encode(byte[] payload, boolean lowerCase) {
      notNull(payload, "payload");

      return Hex.encodeHex(payload, lowerCase);
   }

   public static char[] encode(byte[] payload) {
      notNull(payload, "payload");

      return encode(payload, true);
   }

   public static String encodeToString(byte[] payload, boolean lowerCase) {
      notNull(payload, "payload");

      return Hex.encodeHexString(payload, lowerCase);
   }

   public static String encodeToString(byte[] payload) {
      notNull(payload, "payload");

      return encodeToString(payload, true);
   }

   public static byte[] decode(char[] hex) {
      notNull(hex, "hex");

      try {
         return Hex.decodeHex(hex);
      } catch (DecoderException e) {
         throw new IllegalArgumentException(e.getMessage(), e);
      }
   }

   public static byte[] decode(String hex) {
      notNull(hex, "hex");

      try {
         return Hex.decodeHex(hex);
      } catch (DecoderException e) {
         throw new IllegalArgumentException(e.getMessage(), e);
      }
   }

}
