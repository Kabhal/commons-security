/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.format;

import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.commons.security.hashstorage.HashStorage;
import com.tinubu.commons.security.hashstorage.crypto.Hexa;
import com.tinubu.commons.security.hashstorage.strategy.ComposedHashStrategy;

/**
 * Simple hash encoded with hexadecimal string.
 * The format supports {@link ComposedHashStrategy composed hash algorithms} because there's no metadata in
 * the output, however the parsing is not supported for the same reason.
 */
public class HexHashFormat implements HashFormat {

   private final boolean lowerCase;

   public HexHashFormat(boolean lowerCase) {
      this.lowerCase = lowerCase;
   }

   public HexHashFormat() {
      this(true);
   }

   @Override
   public boolean supportMultipleHashAlgorithms() {
      return true;
   }

   @Override
   public String format(HashStorage hashStorage) {
      notNull(hashStorage, "hashStorage");

      return Hexa.encodeToString(hashStorage.hash(), this.lowerCase);
   }

   @Override
   public HashStorage parse(String hashFormat) {
      notBlank(hashFormat, "hashFormat");

      throw new UnsupportedOperationException("Can't parse hash format with no metadata");
   }
}
