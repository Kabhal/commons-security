/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.strategy;

import static com.tinubu.commons.security.hashstorage.algorithm.Pbkdf2HmacSha256HashAlgorithm.Pbkdf2HmacSha256HashAlgorithmBuilder;

import java.util.StringJoiner;

import com.tinubu.commons.security.hashstorage.algorithm.HashAlgorithm;
import com.tinubu.commons.security.hashstorage.algorithm.Salt;

/**
 * {@link HashStrategy} conforming to OWASP recommendations (August 2021).
 *
 * @see <a href="https://cheatsheetseries.owasp.org/cheatsheets/Password_Storage_Cheat_Sheet.html">OWASP
 *       Password storage cheat sheet</a>
 */
public final class OwaspHashStrategy extends SimpleHashStrategy {

   /** PBKDF2 number of hash iterations to configure. */
   private static final int PBKDF2_ITERATIONS = 310000;

   /** PBKDF2 salt length in bytes. */
   private static final int PBKDF2_SALT_LENGTH = 16;

   /** PBKDF2 key length in bits. */
   private static final int PBKDF2_KEY_LENGTH = 512;

   private OwaspHashStrategy(HashAlgorithm hashAlgorithm) {
      super(hashAlgorithm);
   }

   public static OwaspHashStrategy pbkdf2() {
      return new OwaspHashStrategy(new Pbkdf2HmacSha256HashAlgorithmBuilder()
                                         .iterations(PBKDF2_ITERATIONS)
                                         .salt(Salt.pseudoRandom(PBKDF2_SALT_LENGTH))
                                         .keyLength(PBKDF2_KEY_LENGTH)
                                         .build());
   }

   public static OwaspHashStrategy pbkdf2(Salt salt) {
      if (salt.asBytes().length < PBKDF2_SALT_LENGTH) {
         throw new IllegalArgumentException(String.format("Salt length must be >= %d", PBKDF2_SALT_LENGTH));
      }
      return new OwaspHashStrategy(new Pbkdf2HmacSha256HashAlgorithmBuilder()
                                         .iterations(PBKDF2_ITERATIONS)
                                         .salt(salt)
                                         .keyLength(PBKDF2_KEY_LENGTH)
                                         .build());
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", OwaspHashStrategy.class.getSimpleName() + "[", "]")
            .add("hashAlgorithm=" + hashAlgorithm)
            .toString();
   }
}
