/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.strategy;

import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notEmpty;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.stream.Stream;

import com.tinubu.commons.security.hashstorage.algorithm.HashAlgorithm;

/**
 * {@link HashStrategy} supporting composition of multiple {@link HashStrategy}.
 */
public class ComposedHashStrategy implements HashStrategy {

   protected final List<HashStrategy> preAppliedHashStrategies = new ArrayList<>();
   protected final List<HashStrategy> hashStrategies = new ArrayList<>();

   /**
    * Creates a composed hash strategy configuring a starting hash strategy.
    *
    * @param hashStrategy starting simple hash strategy to configure
    */
   public ComposedHashStrategy(HashStrategy hashStrategy) {
      notNull(hashStrategy, "hashStrategy");

      hashStrategies.add(hashStrategy);
   }

   /**
    * Shortcut for {@link #ComposedHashStrategy(HashStrategy)} using a single hash algorithm.
    *
    * @param hashAlgorithm starting hash algorithm to configure
    */
   public ComposedHashStrategy(HashAlgorithm hashAlgorithm) {
      this(new SimpleHashStrategy(notNull(hashAlgorithm, "hashAlgorithm")));
   }

   /**
    * Builds a {@link ComposedHashStrategy} from a list of hash algorithms, ordered from the first algorithm
    * to the last algorithm applied to payload. For example, the composed strategy {@code sha1(md5(payload))}
    * will be represented as the list {@code [md5, sha1]}.
    *
    * @param hashAlgorithms hash algorithms to configure
    *
    * @return new instance
    */
   public static ComposedHashStrategy ofHashAlgorithms(List<HashAlgorithm> hashAlgorithms) {
      notEmpty(noNullElements(hashAlgorithms, "hashAlgorithms"), "hashAlgorithms");

      ComposedHashStrategy composedHashStrategy = null;
      for (HashAlgorithm hashAlgorithm : hashAlgorithms) {
         if (composedHashStrategy == null) {
            composedHashStrategy = new ComposedHashStrategy(hashAlgorithm);
         } else {
            composedHashStrategy = composedHashStrategy.andThen(hashAlgorithm);
         }
      }

      return composedHashStrategy;
   }

   /**
    * Shortcut for {@link #ofHashAlgorithms(HashAlgorithm...)} using varargs.
    *
    * @param hashAlgorithms hash algorithms to configure
    *
    * @return new instance
    */
   public static ComposedHashStrategy ofHashAlgorithms(HashAlgorithm... hashAlgorithms) {
      return ofHashAlgorithms(Arrays.asList(notNull(hashAlgorithms, "hashAlgorithms")));
   }

   /**
    * Informs that the specified hash strategy has already been applied to payload (so that payload is in
    * fact the resulting hash). This new hash strategy is considered having been applied before all currently
    * configured hash strategies.
    * This hash algorithm won't be applied to payload by {@link #hash(byte[])} function, but will be listed
    * in {@link #hashAlgorithms()}.
    *
    * @param hashStrategy hash strategy to configure
    *
    * @return this hash strategy
    */
   public ComposedHashStrategy preApplied(HashStrategy hashStrategy) {
      notNull(hashStrategy, "hashStrategy");

      preAppliedHashStrategies.add(0, hashStrategy);

      return this;
   }

   /**
    * Shortcut for {@link #preApplied(HashStrategy)} using a single hash algorithm.
    *
    * @param hashAlgorithm hash algorithm to configure
    *
    * @return this hash strategy
    */
   public ComposedHashStrategy preApplied(HashAlgorithm hashAlgorithm) {
      return preApplied(new SimpleHashStrategy(notNull(hashAlgorithm, "hashAlgorithm")));
   }

   /**
    * Configures a supplementary hash strategy to apply before currently configured hash strategies.
    *
    * @param hashStrategy hash hashStrategy to configure
    *
    * @return this hash strategy
    */
   public ComposedHashStrategy compose(HashStrategy hashStrategy) {
      notNull(hashStrategy, "hashStrategy");

      hashStrategies.add(0, hashStrategy);

      return this;
   }

   /**
    * Shortcut for {@link #compose(HashStrategy)} using a single hash algorithm.
    *
    * @param hashAlgorithm hash algorithm to configure
    *
    * @return this hash strategy
    */
   public ComposedHashStrategy compose(HashAlgorithm hashAlgorithm) {
      return compose(new SimpleHashStrategy(notNull(hashAlgorithm, "hashAlgorithm")));
   }

   /**
    * Configures a supplementary hash strategy to apply after currently configured hash strategies.
    *
    * @param hashStrategy hash strategy to configure
    */
   public ComposedHashStrategy andThen(HashStrategy hashStrategy) {
      notNull(hashStrategy, "hashStrategy");

      hashStrategies.add(hashStrategy);

      return this;
   }

   /**
    * Shortcut for {@link #andThen(HashStrategy)} using a single hash algorithm.
    *
    * @param hashAlgorithm hash algorithm to configure
    *
    * @return this hash strategy
    */
   public ComposedHashStrategy andThen(HashAlgorithm hashAlgorithm) {
      return andThen(new SimpleHashStrategy(notNull(hashAlgorithm, "hashAlgorithm")));
   }

   /**
    * {@inheritDoc}
    * <p>
    * If some pre-applied hash algorithms are specified, they are also returned, e.g. : {@code
    * md5(<pre-applied>sha1(<pre-applied>sha256(payload)))} will return the list {@code [<pre-applied>sha256,
    * <pre-applied>sha1, md5]}
    *
    * @return the composed list of hash algorithms
    */
   @Override
   public List<HashAlgorithm> hashAlgorithms() {
      return Stream
            .concat(preAppliedHashStrategies.stream().flatMap(hashStrategy -> hashStrategy.hashAlgorithms().stream()),
                    hashStrategies.stream().flatMap(hashStrategy -> hashStrategy.hashAlgorithms().stream()))
            .collect(toList());
   }

   @Override
   public byte[] hash(byte[] payload) {
      byte[] hash = payload;
      for (HashStrategy hashStrategy : hashStrategies) {
         hash = hashStrategy.hash(hash);
      }
      return hash;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      ComposedHashStrategy that = (ComposedHashStrategy) o;
      return Objects.equals(preAppliedHashStrategies, that.preAppliedHashStrategies) && Objects.equals(
            hashStrategies,
            that.hashStrategies);
   }

   @Override
   public int hashCode() {
      return Objects.hash(preAppliedHashStrategies, hashStrategies);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", ComposedHashStrategy.class.getSimpleName() + "[", "]")
            .add("preAppliedHashStrategies=" + preAppliedHashStrategies)
            .add("hashStrategies=" + hashStrategies)
            .toString();
   }

}
