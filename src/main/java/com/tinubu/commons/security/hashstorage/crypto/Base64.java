/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.crypto;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Base64.Encoder;

public final class Base64 {

   private static final java.util.Base64.Encoder BASE64_ENCODER = java.util.Base64.getEncoder();
   private static final java.util.Base64.Encoder BASE64_ENCODER_WITHOUT_PADDING =
         BASE64_ENCODER.withoutPadding();
   private static final java.util.Base64.Decoder BASE64_DECODER = java.util.Base64.getDecoder();

   private Base64() {}

   public static byte[] encode(byte[] payload, boolean padding) {
      notNull(payload, "payload");

      return base64Encoder(padding).encode(payload);
   }

   public static byte[] encode(byte[] payload) {
      notNull(payload, "payload");

      return encode(payload, true);
   }

   public static String encodeToString(byte[] payload, boolean padding) {
      notNull(payload, "payload");

      return base64Encoder(padding).encodeToString(payload);
   }

   public static String encodeToString(byte[] payload) {
      notNull(payload, "payload");

      return encodeToString(payload, true);
   }

   public static byte[] decode(byte[] base64) {
      notNull(base64, "base64");

      return BASE64_DECODER.decode(base64);
   }

   public static byte[] decode(String base64) {
      notNull(base64, "base64");

      return BASE64_DECODER.decode(base64);
   }

   private static Encoder base64Encoder(boolean padding) {
      return padding ? BASE64_ENCODER : BASE64_ENCODER_WITHOUT_PADDING;
   }

}
