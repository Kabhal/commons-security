/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.algorithm;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.lang.Integer.parseInt;
import static java.util.Collections.singletonList;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;

import javax.crypto.SecretKeyFactory;

/**
 * {@link HashAlgorithm} implementation using Password-Based Key Derivation Function 2 (PBKDF2) with
 * HMAC-SHA-512 pseudorandom function.
 */
public class Pbkdf2HmacSha512HashAlgorithm extends SecretKeyHashAlgorithm implements HashAlgorithm {
   private static final List<String> ALGORITHM_NAMES = singletonList("pbkdf2hmacsha512");
   /** PBKDF2 algorithm {@link SecretKeyFactory} instance name. */
   private static final String PBKDF2_HMACSHA512_ALGORITHM = "PBKDF2WithHmacSHA512";
   private static final int DEFAULT_ITERATIONS = 310000;
   private static final int DEFAULT_KEY_LENGTH = 512;
   private static final int DEFAULT_SALT_LENGTH = 16;

   private Pbkdf2HmacSha512HashAlgorithm(Pbkdf2HmacSha512HashAlgorithmBuilder builder) {
      super(PBKDF2_HMACSHA512_ALGORITHM,
            validateSalt(builder.salt),
            validateIterations(builder.iterations),
            validateKeyLength(builder.keyLength),
            builder.payloadCharset);
   }

   private static Salt validateSalt(Salt salt) {
      return nullable(salt).orElseGet(() -> Salt.pseudoRandom(DEFAULT_SALT_LENGTH));
   }

   private static int validateIterations(Integer iterations) {
      return nullable(iterations).orElse(DEFAULT_ITERATIONS);
   }

   private static int validateKeyLength(Integer keyLength) {
      return nullable(keyLength).orElse(DEFAULT_KEY_LENGTH);
   }

   @Override
   public List<String> algorithmNames() {
      return ALGORITHM_NAMES;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Pbkdf2HmacSha512HashAlgorithm that = (Pbkdf2HmacSha512HashAlgorithm) o;
      return iterations == that.iterations
             && keyLength == that.keyLength
             && Objects.equals(salt, that.salt)
             && Objects.equals(payloadCharset, that.payloadCharset);
   }

   @Override
   public int hashCode() {
      return Objects.hash(salt, iterations, keyLength, payloadCharset);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", Pbkdf2HmacSha512HashAlgorithm.class.getSimpleName() + "[", "]")
            .add("salt=" + salt)
            .add("iterations=" + iterations)
            .add("keyLength=" + keyLength)
            .add("payloadCharset=" + payloadCharset)
            .toString();
   }

   public static class Pbkdf2HmacSha512HashAlgorithmBuilder
         extends SecretKeyHashAlgorithmBuilder<Pbkdf2HmacSha512HashAlgorithmBuilder> {
      public Pbkdf2HmacSha512HashAlgorithm build() {
         return new Pbkdf2HmacSha512HashAlgorithm(this);
      }
   }

   public static class Pbkdf2HmacSha512HashAlgorithmProvider implements HashAlgorithmProvider {

      @Override
      public List<String> algorithmNames() {
         return ALGORITHM_NAMES;
      }

      @Override
      public Pbkdf2HmacSha512HashAlgorithm fromFormat(Salt salt, Map<String, String> parameters) {
         notNull(parameters, "parameters");

         return new Pbkdf2HmacSha512HashAlgorithmBuilder()
               .salt(requireSalt(salt))
               .iterations(parseInt(requireParameter(parameters, ITERATIONS_PARAMETER)))
               .keyLength(parseInt(requireParameter(parameters, KEY_LENGTH_PARAMETER)))
               .build();
      }
   }

}
