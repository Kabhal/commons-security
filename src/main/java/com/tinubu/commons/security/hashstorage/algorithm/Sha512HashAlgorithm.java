/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.algorithm;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.security.MessageDigest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * SHA-512 {@link HashAlgorithm} implementation.
 */
public class Sha512HashAlgorithm extends MessageDigestHashAlgorithm implements HashAlgorithm {
   private static final List<String> ALGORITHM_NAMES = Arrays.asList("sha512", "6");

   /**
    * SHA-1 algorithm {@link MessageDigest} instance name.
    */
   public static final String SHA512_ALGORITHM = "SHA-512";

   private Sha512HashAlgorithm(Sha512HashAlgorithmBuilder builder) {
      super(SHA512_ALGORITHM, builder.salt);
   }

   @Override
   public List<String> algorithmNames() {
      return ALGORITHM_NAMES;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Sha512HashAlgorithm that = (Sha512HashAlgorithm) o;
      return Objects.equals(salt, that.salt);
   }

   @Override
   public int hashCode() {
      return Objects.hash(salt);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", Sha512HashAlgorithm.class.getSimpleName() + "[", "]").add("salt=" + salt).toString();
   }

   public static class Sha512HashAlgorithmBuilder
         extends MessageDigestHashAlgorithmBuilder<Sha512HashAlgorithmBuilder> {
      @Override
      public Sha512HashAlgorithm build() {
         return new Sha512HashAlgorithm(this);
      }
   }

   public static class Sha512HashAlgorithmProvider implements HashAlgorithmProvider {

      @Override
      public List<String> algorithmNames() {
         return ALGORITHM_NAMES;
      }

      @Override
      public Sha512HashAlgorithm fromFormat(Salt salt, Map<String, String> parameters) {
         notNull(parameters, "parameters");

         return new Sha512HashAlgorithmBuilder().salt(salt).build();
      }
   }

}
