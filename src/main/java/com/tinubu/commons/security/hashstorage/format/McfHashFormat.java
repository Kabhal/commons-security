/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.format;

import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.commons.security.hashstorage.HashStorage;

/**
 * Modular Crypt Format (MCF) support.
 * The format is simply {@code $<identifier>$<hash>[$<parameter>[$<parameter>]...]}
 */
public class McfHashFormat implements HashFormat {

   @Override
   public boolean supportMultipleHashAlgorithms() {
      return false;
   }

   @Override
   public String format(HashStorage hashStorage) {
      notNull(hashStorage, "hashStorage");

      throw new UnsupportedOperationException("FIXME");
   }

   @Override
   public HashStorage parse(String hashFormat) {
      notBlank(hashFormat, "hashFormat");

      throw new UnsupportedOperationException("FIXME");
   }
}
