/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage;

import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notEmpty;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import com.tinubu.commons.security.hashstorage.algorithm.HashAlgorithm;
import com.tinubu.commons.security.hashstorage.algorithm.Salt;

public final class TestUtils {

   public static final Charset TEST_CHARSET = StandardCharsets.UTF_8;

   private TestUtils() {}

   /**
    * Hashes payload with specified hash algorithms.
    *
    * @param payload payload to hash
    * @param hashAlgorithms hash algorithms from first to last to apply to payload
    *
    * @return hash
    */
   public static byte[] hash(byte[] payload, HashAlgorithm... hashAlgorithms) {
      notEmpty(noNullElements(hashAlgorithms, "hashAlgorithms"), "hashAlgorithms");

      byte[] hash = null;
      for (HashAlgorithm hashAlgorithm : hashAlgorithms) {
         if (hash == null) {
            hash = hashAlgorithm.hash(payload);
         } else {
            hash = hashAlgorithm.hash(hash);
         }
      }
      return hash;
   }

   /**
    * Assumes {@link StandardCharsets#UTF_8} for payload value.
    *
    * @param payload payload
    *
    * @return payload as byte array
    */
   public static byte[] payload(String payload) {
      return payload.getBytes(TEST_CHARSET);
   }

   /**
    * Assumes {@link StandardCharsets#UTF_8} for salt value.
    *
    * @param salt salt
    *
    * @return salt as byte array
    */
   public static Salt salt(String salt) {
      return Salt.ofString(salt, TEST_CHARSET);
   }

}
