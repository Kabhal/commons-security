/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.algorithm;

import static com.tinubu.commons.security.hashstorage.TestUtils.payload;
import static java.util.Collections.emptyMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.security.hashstorage.algorithm.Sha512HashAlgorithm.Sha512HashAlgorithmBuilder;
import com.tinubu.commons.security.hashstorage.algorithm.Sha512HashAlgorithm.Sha512HashAlgorithmProvider;

class Sha512HashAlgorithmTest {

   @Nested
   public class Instance {

      @Test
      public void testInstanceWhenNominal() {
         Sha512HashAlgorithm hashAlgorithm = new Sha512HashAlgorithmBuilder().build();

         assertThat(hashAlgorithm.algorithmNames()).containsExactly("sha512", "6");
         assertThat(hashAlgorithm.salt()).isEmpty();
         assertThat(hashAlgorithm.parameters()).isEmpty();
      }

      @Test
      public void testInstanceWhenSalt() {
         Sha512HashAlgorithm hashAlgorithm =
               new Sha512HashAlgorithmBuilder().salt(Salt.ofBytes(new byte[] { 84, -10, 22, 34 })).build();

         assertThat(hashAlgorithm.salt()).get().satisfies(salt -> assertThat(salt.asBytes()).hasSize(4));
         assertThat(hashAlgorithm.parameters()).isEmpty();
      }

      @Test
      public void testInstanceWhenNullSalt() {
         Sha512HashAlgorithm hashAlgorithm = new Sha512HashAlgorithmBuilder().salt(null).build();

         assertThat(hashAlgorithm.salt()).isEmpty();
         assertThat(hashAlgorithm.parameters()).isEmpty();
      }
   }

   @Nested
   public class Provider {

      @Test
      public void testProviderAlgorithmNames() {
         assertThat(new Sha512HashAlgorithmProvider().algorithmNames()).containsExactlyInAnyOrder("sha512",
                                                                                                  "6");
      }

      @Test
      public void testProviderFromFormatWhenNominal() {
         assertThat(new Sha512HashAlgorithmProvider().fromFormat(Salt.ofString("salt"),
                                                                 emptyMap())).isNotNull();
      }

      @Test
      public void testProviderFromFormatWhenOptionalSalt() {
         assertThat(new Sha512HashAlgorithmProvider().fromFormat(null, emptyMap())).isNotNull();
      }

      @Test
      public void testProviderFromFormatWhenBadParameters() {
         assertThatNullPointerException()
               .isThrownBy(() -> new Sha512HashAlgorithmProvider().fromFormat(Salt.ofString("salt"), null))
               .withMessage("'parameters' must not be null");
      }

   }

   @Nested
   public class EqualityAndToString {

      @Test
      public void testEquals() {
         Sha512HashAlgorithm hashAlgorithm1 = new Sha512HashAlgorithmBuilder().build();
         Sha512HashAlgorithm hashAlgorithm2 = new Sha512HashAlgorithmBuilder().build();
         Sha512HashAlgorithm hashAlgorithm3 =
               new Sha512HashAlgorithmBuilder().salt(Salt.pseudoRandom(4)).build();

         assertThat(hashAlgorithm1).isEqualTo(hashAlgorithm2);
         assertThat(hashAlgorithm1).hasSameHashCodeAs(hashAlgorithm2);

         assertThat(hashAlgorithm1).isNotEqualTo(hashAlgorithm3);
      }

      @Test
      public void testToString() {
         assertThat(new Sha512HashAlgorithmBuilder()
                          .salt(Salt.ofBytes(new byte[] { 84, -10, 22, 34 }))
                          .build()).hasToString("Sha512HashAlgorithm[salt=Salt[salt=[84, -10, 22, 34]]]");
      }

   }

   @Nested
   public class Hash {

      @Test
      public void testHashWhenNominal() {
         Sha512HashAlgorithm hashAlgorithm = new Sha512HashAlgorithmBuilder().build();

         assertThat(hashAlgorithm.hash(payload("payload"))).isEqualTo(new byte[] {
               112, -77, 60, -23, -55, 4, 126, 48, -7, 23, -25, -22, 19, -28, 47, 119, 103, 0, -116, 63, 79, -100, -101, -81, 73, -28, 57, 15, -58, 37, 84, -98, -106, 37, -18, -29, -101, -108, 84, 80,
               116, -24, -95, -126, 76, -13, -14, 56, 70, 59, 17, -68, 3, -39, 115, 72, -32, -4, 41, -103, -54, 31, -1, 127 });
      }

      @Test
      public void testHashWhenEmptyPayload() {
         Sha512HashAlgorithm hashAlgorithm = new Sha512HashAlgorithmBuilder().build();

         assertThat(hashAlgorithm.hash(payload(""))).isEqualTo(new byte[] {
               -49, -125, -31, 53, 126, -17, -72, -67, -15, 84, 40, 80, -42, 109, -128, 7, -42, 32, -28, 5, 11, 87, 21, -36, -125, -12, -87, 33, -45, 108, -23, -50, 71, -48, -47, 60, 93, -123, -14, -80,
               -1, -125, 24, -46, -121, 126, -20, 47, 99, -71, 49, -67, 71, 65, 122, -127, -91, 56, 50, 122, -7, 39, -38, 62 });
      }

      @Test
      public void testHashWhenNullPayload() {
         Sha512HashAlgorithm hashAlgorithm = new Sha512HashAlgorithmBuilder().build();

         assertThatNullPointerException().isThrownBy(() -> hashAlgorithm.hash(null)).withMessage("'payload' must not be null");
      }

   }

   @Nested
   public class ArchitecturalDesign {

      private static final int NB_ITERATIONS = 10;

      @Test
      public void testHashAlgorithmIsReusable() {
         List<Payload> expectedHashes = new ArrayList<>();
         IntStream
               .range(0, NB_ITERATIONS)
               .forEach(i -> expectedHashes.add(new Payload(new Sha512HashAlgorithmBuilder()
                                                                  .build()
                                                                  .hash(("payload-" + i).getBytes(
                                                                        StandardCharsets.UTF_8)))));

         List<Payload> hashes = new ArrayList<>();
         Sha512HashAlgorithm hashAlgorithm = new Sha512HashAlgorithmBuilder().build();
         IntStream
               .range(0, NB_ITERATIONS)
               .forEach(i -> hashes.add(new Payload(hashAlgorithm.hash(("payload-" + i).getBytes(
                     StandardCharsets.UTF_8)))));

         assertThat(hashes).isEqualTo(expectedHashes);
      }

      class Payload {
         byte[] hash;

         public Payload(byte[] hash) {
            this.hash = hash;
         }

         @Override
         public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Payload payload = (Payload) o;
            return Arrays.equals(hash, payload.hash);
         }

         @Override
         public int hashCode() {
            return Arrays.hashCode(hash);
         }
      }

   }

   @Nested
   public class Performance extends PerformanceTest {

      public Performance() {
         super(new Sha512HashAlgorithmBuilder().build());
      }

   }

}