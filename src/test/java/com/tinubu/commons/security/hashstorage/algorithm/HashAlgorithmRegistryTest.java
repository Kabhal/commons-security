/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.algorithm;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

import java.security.MessageDigest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.security.hashstorage.algorithm.HashAlgorithmRegistryTest.TestHashAlgorithm.TestHashAlgorithmProvider;

public class HashAlgorithmRegistryTest {

   @Test
   public void testHashRegistrySingleton() {
      HashAlgorithmRegistry instance1 = HashAlgorithmRegistry.instance();
      HashAlgorithmRegistry instance2 = HashAlgorithmRegistry.instance();

      assertThat(instance1).isSameAs(instance2);
   }

   @Test
   public void testHashRegistryWhenUnknownProvider() {
      assertThat(HashAlgorithmRegistry.instance().hashAlgorithmProvider("missing")).isEmpty();
   }

   @Test
   public void testHashRegistryWhenNormalizedIndex() {
      HashAlgorithmRegistry registry = HashAlgorithmRegistry.instance();

      registry.clearRegisteredHashAlgorithmProviders();
      registry.registerHashAlgorithm(new TestHashAlgorithmProvider(singletonList("$hash-1$")));

      assertThat(registry.hashAlgorithmProvider("$hash-1$"))
            .get()
            .extracting(HashAlgorithmProvider::algorithmNames)
            .isEqualTo(singletonList("$hash-1$"));
      assertThat(registry.hashAlgorithmProvider("hash1"))
            .get()
            .extracting(HashAlgorithmProvider::algorithmNames)
            .isEqualTo(singletonList("$hash-1$"));
      assertThat(registry.hashAlgorithmProvider("-$-hash-$-1-$-"))
            .get()
            .extracting(HashAlgorithmProvider::algorithmNames)
            .isEqualTo(singletonList("$hash-1$"));
   }

   @Test
   public void testRegisterHashAlgorithm() {
      HashAlgorithmRegistry algorithmRegistry = HashAlgorithmRegistry.instance();

      algorithmRegistry.registerHashAlgorithm(new TestHashAlgorithmProvider());

      assertThat(algorithmRegistry.hashAlgorithmProvider("test")).isNotNull();
      assertThat(algorithmRegistry.hashAlgorithmProvider("testalias")).isNotNull();
      assertThat(algorithmRegistry.registeredHashAlgorithmProviders())
            .flatExtracting(HashAlgorithmProvider::algorithmNames)
            .contains("test", "testalias");
   }

   static class TestHashAlgorithm extends MessageDigestHashAlgorithm implements HashAlgorithm {
      private static final List<String> ALGORITHM_NAMES = Arrays.asList("test", "testalias");

      /**
       * MD5 algorithm {@link MessageDigest} instance name.
       */
      public static final String MD5_ALGORITHM = "MD5";

      private TestHashAlgorithm(TestHashAlgorithmBuilder builder) {
         super(MD5_ALGORITHM, builder.salt);
      }

      @Override
      public List<String> algorithmNames() {
         return ALGORITHM_NAMES;
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         return super.equals(o);
      }

      @Override
      public int hashCode() {
         return Objects.hash(super.hashCode());
      }

      public static class TestHashAlgorithmBuilder
            extends MessageDigestHashAlgorithmBuilder<TestHashAlgorithmBuilder> {
         @Override
         public TestHashAlgorithm build() {
            return new TestHashAlgorithm(this);
         }
      }

      public static class TestHashAlgorithmProvider implements HashAlgorithmProvider {

         private List<String> algorithmNames;

         public TestHashAlgorithmProvider(List<String> algorithmNames) {
            this.algorithmNames = notNull(algorithmNames);
         }

         public TestHashAlgorithmProvider() {
            this(ALGORITHM_NAMES);
         }

         @Override
         public List<String> algorithmNames() {
            return algorithmNames;
         }

         @Override
         public TestHashAlgorithm fromFormat(Salt salt, Map<String, String> parameters) {
            notNull(parameters, "parameters");

            return new TestHashAlgorithmBuilder().salt(salt).build();
         }
      }

   }

}