/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.algorithm;

import static com.tinubu.commons.security.hashstorage.TestUtils.payload;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.security.hashstorage.algorithm.Pbkdf2HmacSha512HashAlgorithm.Pbkdf2HmacSha512HashAlgorithmBuilder;
import com.tinubu.commons.security.hashstorage.algorithm.Pbkdf2HmacSha512HashAlgorithm.Pbkdf2HmacSha512HashAlgorithmProvider;
import com.tinubu.commons.security.hashstorage.algorithm.Sha1HashAlgorithm.Sha1HashAlgorithmBuilder;

class Pbkdf2HmacSha512HashAlgorithmTest {

   @Nested
   public class Instance {

      @Test
      public void testInstanceWhenNominal() {
         Pbkdf2HmacSha512HashAlgorithm hashAlgorithm = new Pbkdf2HmacSha512HashAlgorithmBuilder().build();

         assertThat(hashAlgorithm.algorithmNames()).containsExactly("pbkdf2hmacsha512");
         assertThat(hashAlgorithm.salt()).get().satisfies(salt -> assertThat(salt.asBytes()).hasSize(16));
         assertThat(hashAlgorithm.iterations()).isEqualTo(310000);
         assertThat(hashAlgorithm.keyLength()).isEqualTo(512);
         assertThat(hashAlgorithm.parameters()).containsExactly(Pair.of("it", "310000"), Pair.of("kl", "512"));
      }

      @Test
      public void testInstanceWhenBadParameters() {
         assertThatIllegalArgumentException()
               .isThrownBy(() -> new Pbkdf2HmacSha512HashAlgorithmBuilder().iterations(0).build())
               .withMessage("'iterations' must be > 0");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> new Pbkdf2HmacSha512HashAlgorithmBuilder().keyLength(0).build())
               .withMessage("'keyLength' must be > 0");
      }

      @Test
      public void testInstanceWhenSalt() {
         Pbkdf2HmacSha512HashAlgorithm hashAlgorithm = new Pbkdf2HmacSha512HashAlgorithmBuilder()
               .salt(Salt.ofBytes(new byte[] { 84, -10, 22, 34 }))
               .build();

         assertThat(hashAlgorithm.salt()).get().satisfies(salt -> assertThat(salt.asBytes()).hasSize(4));
         assertThat(hashAlgorithm.parameters()).containsExactly(Pair.of("it", "310000"), Pair.of("kl", "512"));
      }

      @Test
      public void testInstanceWhenNullSalt() {
         Pbkdf2HmacSha512HashAlgorithm hashAlgorithm =
               new Pbkdf2HmacSha512HashAlgorithmBuilder().salt(null).build();

         assertThat(hashAlgorithm.salt()).get().satisfies(salt -> assertThat(salt.asBytes()).hasSize(16));
         assertThat(hashAlgorithm.parameters()).containsExactly(Pair.of("it", "310000"), Pair.of("kl", "512"));
      }

   }

   @Nested
   public class Provider {

      @Test
      public void testProviderAlgorithmNames() {
         assertThat(new Pbkdf2HmacSha512HashAlgorithmProvider().algorithmNames()).containsExactlyInAnyOrder(
               "pbkdf2hmacsha512");
      }

      @Test
      public void testProviderFromFormatWhenNominal() {
         assertThat(new Pbkdf2HmacSha512HashAlgorithmProvider().fromFormat(Salt.ofString("salt"), new HashMap<String, String>() {{
            put("it", "310000");
            put("kl", "512");
         }})).isNotNull();
      }

      @Test
      public void testProviderFromFormatWhenBadParameters() {
         assertThatIllegalArgumentException()
               .isThrownBy(() -> new Pbkdf2HmacSha512HashAlgorithmProvider().fromFormat(null, new HashMap<String, String>() {{
                  put("it", "310000");
                  put("kl", "512");
               }}))
               .withMessage("No salt set");
         assertThatNullPointerException()
               .isThrownBy(() -> new Pbkdf2HmacSha512HashAlgorithmProvider().fromFormat(Salt.ofString("salt"),
                                                                                        null))
               .withMessage("'parameters' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> new Pbkdf2HmacSha512HashAlgorithmProvider().fromFormat(Salt.ofString("salt"),
                                                                                        new HashMap<String, String>() {{
                                                                                           put("kl", "512");
                                                                                        }}))
               .withMessage("No 'it' parameter set");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> new Pbkdf2HmacSha512HashAlgorithmProvider().fromFormat(Salt.ofString("salt"),
                                                                                        new HashMap<String, String>() {{
                                                                                           put("it", "310000");
                                                                                        }}))
               .withMessage("No 'kl' parameter set");
      }

   }

   @Nested
   public class EqualityAndToString {

      @Test
      public void testEquals() {
         Pbkdf2HmacSha512HashAlgorithm hashAlgorithm1 = new Pbkdf2HmacSha512HashAlgorithmBuilder()
               .salt(Salt.ofBytes(new byte[] { 84, -10, 22, 34 }))
               .build();
         Pbkdf2HmacSha512HashAlgorithm hashAlgorithm2 = new Pbkdf2HmacSha512HashAlgorithmBuilder()
               .salt(Salt.ofBytes(new byte[] { 84, -10, 22, 34 }))
               .build();
         Pbkdf2HmacSha512HashAlgorithm hashAlgorithm3 =
               new Pbkdf2HmacSha512HashAlgorithmBuilder().salt(Salt.pseudoRandom(4)).build();

         assertThat(hashAlgorithm1).isEqualTo(hashAlgorithm2);
         assertThat(hashAlgorithm1).hasSameHashCodeAs(hashAlgorithm2);

         assertThat(hashAlgorithm1).isNotEqualTo(hashAlgorithm3);
      }

      @Test
      public void testToString() {
         assertThat(new Pbkdf2HmacSha512HashAlgorithmBuilder()
                          .keyLength(128)
                          .iterations(1000)
                          .salt(Salt.ofBytes(new byte[] { 84, -10, 22, 34 }))
                          .build()).hasToString(
               "Pbkdf2HmacSha512HashAlgorithm[salt=Salt[salt=[84, -10, 22, 34]], iterations=1000, keyLength=128, payloadCharset=UTF-8]");
      }

   }

   @Nested
   public class Hash {

      @Test
      public void testHashWhenNominal() {
         Pbkdf2HmacSha512HashAlgorithm hashAlgorithm = new Pbkdf2HmacSha512HashAlgorithmBuilder()
               .keyLength(128)
               .salt(Salt.ofBytes(new byte[] { 84, -10, 22, 34 }))
               .build();

         assertThat(hashAlgorithm.hash(payload("payload"))).isEqualTo(new byte[] {
               -83, -107, -56, 11, 91, 60, 67, 88, -108, 102, -115, -6, -57, -10, 51, 52 });
      }

      @Test
      public void testHashWhenEmptyPayload() {
         Pbkdf2HmacSha512HashAlgorithm hashAlgorithm = new Pbkdf2HmacSha512HashAlgorithmBuilder()
               .keyLength(128)
               .salt(Salt.ofBytes(new byte[] { 84, -10, 22, 34 }))
               .build();

         assertThat(hashAlgorithm.hash(payload(""))).isEqualTo(new byte[] {
               -118, 38, 106, -82, 76, -116, 110, -69, 74, 63, -101, 64, -96, -36, 20, -74 });
      }

      @Test
      public void testHashWhenNullPayload() {
         Pbkdf2HmacSha512HashAlgorithm hashAlgorithm = new Pbkdf2HmacSha512HashAlgorithmBuilder().build();

         assertThatNullPointerException().isThrownBy(() -> hashAlgorithm.hash(null)).withMessage("'payload' must not be null");
      }

   }

   @Nested
   public class ArchitecturalRules {

      @Test
      public void testPayloadEncoding() {
         Pbkdf2HmacSha512HashAlgorithm hashAlgorithmUtf8 =
               new Pbkdf2HmacSha512HashAlgorithmBuilder().payloadCharset(StandardCharsets.UTF_8).build();

         assertThat(hashAlgorithmUtf8.payloadCharset()).isEqualTo(StandardCharsets.UTF_8);
         assertThat(hashAlgorithmUtf8.hash("payload".getBytes(StandardCharsets.UTF_8))).isNotNull();
         assertThatIllegalArgumentException()
               .isThrownBy(() -> hashAlgorithmUtf8.hash("payload".getBytes(StandardCharsets.UTF_16)))
               .withMessage(
                     "Input payload must be encoded in UTF-8. You can configure an alternative payload charset");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> hashAlgorithmUtf8.hash(new Sha1HashAlgorithmBuilder()
                                                              .build()
                                                              .hash(payload("payload"))))
               .withMessage(
                     "Input payload must be encoded in UTF-8. You can configure an alternative payload charset");

         Pbkdf2HmacSha512HashAlgorithm hashAlgorithmUtf16 =
               new Pbkdf2HmacSha512HashAlgorithmBuilder().payloadCharset(StandardCharsets.UTF_16).build();

         assertThat(hashAlgorithmUtf16.payloadCharset()).isEqualTo(StandardCharsets.UTF_16);
         assertThat(hashAlgorithmUtf16.hash("payload".getBytes(StandardCharsets.UTF_16))).isNotNull();
         assertThatIllegalArgumentException()
               .isThrownBy(() -> hashAlgorithmUtf16.hash("payload".getBytes(StandardCharsets.UTF_8)))
               .withMessage(
                     "Input payload must be encoded in UTF-16. You can configure an alternative payload charset");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> hashAlgorithmUtf16.hash(new Sha1HashAlgorithmBuilder()
                                                               .build()
                                                               .hash(payload("payload"))))
               .withMessage(
                     "Input payload must be encoded in UTF-16. You can configure an alternative payload charset");
      }

      private static final int NB_ITERATIONS = 5;

      @Test
      public void testHashAlgorithmIsReusable() {
         List<Payload> expectedHashes = new ArrayList<>();
         IntStream
               .range(0, NB_ITERATIONS)
               .forEach(i -> expectedHashes.add(new Payload(new Pbkdf2HmacSha512HashAlgorithmBuilder()
                                                                  .salt(Salt.ofString("salt"))
                                                                  .build()
                                                                  .hash(("payload-" + i).getBytes(
                                                                        StandardCharsets.UTF_8)))));

         List<Payload> hashes = new ArrayList<>();
         Pbkdf2HmacSha512HashAlgorithm hashAlgorithm =
               new Pbkdf2HmacSha512HashAlgorithmBuilder().salt(Salt.ofString("salt")).build();
         IntStream
               .range(0, NB_ITERATIONS)
               .forEach(i -> hashes.add(new Payload(hashAlgorithm.hash(("payload-" + i).getBytes(
                     StandardCharsets.UTF_8)))));

         assertThat(hashes).isEqualTo(expectedHashes);
      }

      class Payload {
         byte[] hash;

         public Payload(byte[] hash) {
            this.hash = hash;
         }

         @Override
         public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Payload payload = (Payload) o;
            return Arrays.equals(hash, payload.hash);
         }

         @Override
         public int hashCode() {
            return Arrays.hashCode(hash);
         }
      }

   }

   @Nested
   public class Performance extends PerformanceTest {

      public Performance() {
         super(new Pbkdf2HmacSha512HashAlgorithmBuilder().build());
      }

   }

}