/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.algorithm;

import static com.tinubu.commons.security.hashstorage.TestUtils.payload;
import static java.util.Collections.emptyMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.security.hashstorage.algorithm.Sha256HashAlgorithm.Sha256HashAlgorithmBuilder;
import com.tinubu.commons.security.hashstorage.algorithm.Sha256HashAlgorithm.Sha256HashAlgorithmProvider;

class Sha256HashAlgorithmTest {

   @Nested
   public class Instance {

      @Test
      public void testInstanceWhenNominal() {
         Sha256HashAlgorithm hashAlgorithm = new Sha256HashAlgorithmBuilder().build();

         assertThat(hashAlgorithm.algorithmNames()).containsExactly("sha256", "5");
         assertThat(hashAlgorithm.salt()).isEmpty();
         assertThat(hashAlgorithm.parameters()).isEmpty();
      }

      @Test
      public void testInstanceWhenSalt() {
         Sha256HashAlgorithm hashAlgorithm =
               new Sha256HashAlgorithmBuilder().salt(Salt.ofBytes(new byte[] { 84, -10, 22, 34 })).build();

         assertThat(hashAlgorithm.salt()).get().satisfies(salt -> assertThat(salt.asBytes()).hasSize(4));
         assertThat(hashAlgorithm.parameters()).isEmpty();
      }

      @Test
      public void testInstanceWhenNullSalt() {
         Sha256HashAlgorithm hashAlgorithm = new Sha256HashAlgorithmBuilder().salt(null).build();

         assertThat(hashAlgorithm.salt()).isEmpty();
         assertThat(hashAlgorithm.parameters()).isEmpty();
      }
   }

   @Nested
   public class Provider {

      @Test
      public void testProviderAlgorithmNames() {
         assertThat(new Sha256HashAlgorithmProvider().algorithmNames()).containsExactlyInAnyOrder("sha256",
                                                                                                  "5");
      }

      @Test
      public void testProviderFromFormatWhenNominal() {
         assertThat(new Sha256HashAlgorithmProvider().fromFormat(Salt.ofString("salt"),
                                                                 emptyMap())).isNotNull();
      }

      @Test
      public void testProviderFromFormatWhenOptionalSalt() {
         assertThat(new Sha256HashAlgorithmProvider().fromFormat(null, emptyMap())).isNotNull();
      }

      @Test
      public void testProviderFromFormatWhenBadParameters() {
         assertThatNullPointerException()
               .isThrownBy(() -> new Sha256HashAlgorithmProvider().fromFormat(Salt.ofString("salt"), null))
               .withMessage("'parameters' must not be null");
      }

   }

   @Nested
   public class EqualityAndToString {

      @Test
      public void testEquals() {
         Sha256HashAlgorithm hashAlgorithm1 = new Sha256HashAlgorithmBuilder().build();
         Sha256HashAlgorithm hashAlgorithm2 = new Sha256HashAlgorithmBuilder().build();
         Sha256HashAlgorithm hashAlgorithm3 =
               new Sha256HashAlgorithmBuilder().salt(Salt.pseudoRandom(4)).build();

         assertThat(hashAlgorithm1).isEqualTo(hashAlgorithm2);
         assertThat(hashAlgorithm1).hasSameHashCodeAs(hashAlgorithm2);

         assertThat(hashAlgorithm1).isNotEqualTo(hashAlgorithm3);
      }

      @Test
      public void testToString() {
         assertThat(new Sha256HashAlgorithmBuilder()
                          .salt(Salt.ofBytes(new byte[] { 84, -10, 22, 34 }))
                          .build()).hasToString("Sha256HashAlgorithm[salt=Salt[salt=[84, -10, 22, 34]]]");
      }

   }

   @Nested
   public class Hash {

      @Test
      public void testHashWhenNominal() {
         Sha256HashAlgorithm hashAlgorithm = new Sha256HashAlgorithmBuilder().build();

         assertThat(hashAlgorithm.hash(payload("payload"))).isEqualTo(new byte[] {
               35, -97, 89, -19, 85, -25, 55, -57, 113, 71, -49, 85, -83, 12, 27, 3, 11, 109, 126, -25, 72, -89, 66, 105, 82, -7, -72, 82, -43, -87, 53, -27 });
      }

      @Test
      public void testHashWhenEmptyPayload() {
         Sha256HashAlgorithm hashAlgorithm = new Sha256HashAlgorithmBuilder().build();

         assertThat(hashAlgorithm.hash(payload(""))).isEqualTo(new byte[] {
               -29, -80, -60, 66, -104, -4, 28, 20, -102, -5, -12, -56, -103, 111, -71, 36, 39, -82, 65, -28,
               100, -101, -109, 76, -92, -107, -103, 27, 120, 82, -72, 85 });
      }

      @Test
      public void testHashWhenNullPayload() {
         Sha256HashAlgorithm hashAlgorithm = new Sha256HashAlgorithmBuilder().build();

         assertThatNullPointerException().isThrownBy(() -> hashAlgorithm.hash(null)).withMessage("'payload' must not be null");
      }

   }

   @Nested
   public class ArchitecturalDesign {

      private static final int NB_ITERATIONS = 10;

      @Test
      public void testHashAlgorithmIsReusable() {
         List<Payload> expectedHashes = new ArrayList<>();
         IntStream
               .range(0, NB_ITERATIONS)
               .forEach(i -> expectedHashes.add(new Payload(new Sha256HashAlgorithmBuilder()
                                                                  .build()
                                                                  .hash(("payload-" + i).getBytes(
                                                                        StandardCharsets.UTF_8)))));

         List<Payload> hashes = new ArrayList<>();
         Sha256HashAlgorithm hashAlgorithm = new Sha256HashAlgorithmBuilder().build();
         IntStream
               .range(0, NB_ITERATIONS)
               .forEach(i -> hashes.add(new Payload(hashAlgorithm.hash(("payload-" + i).getBytes(
                     StandardCharsets.UTF_8)))));

         assertThat(hashes).isEqualTo(expectedHashes);
      }

      class Payload {
         byte[] hash;

         public Payload(byte[] hash) {
            this.hash = hash;
         }

         @Override
         public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Payload payload = (Payload) o;
            return Arrays.equals(hash, payload.hash);
         }

         @Override
         public int hashCode() {
            return Arrays.hashCode(hash);
         }
      }

   }

   @Nested
   public class Performance extends PerformanceTest {

      public Performance() {
         super(new Sha256HashAlgorithmBuilder().build());
      }

   }

}