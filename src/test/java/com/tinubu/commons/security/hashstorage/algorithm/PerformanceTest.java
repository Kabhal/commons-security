/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.algorithm;

import static com.tinubu.commons.security.hashstorage.TestUtils.payload;
import static java.lang.Long.sum;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.Test;

public abstract class PerformanceTest {

   /** Number of test iterations to compute a mean. */
   public static final int PERFORMANCE_TEST_ITERATIONS = 10;

   /** Maximum hash duration in milliseconds, using hash algorithm default parameters. */
   public static final int MAX_HASH_DURATION = 3000;

   private final HashAlgorithm hashAlgorithm;

   public PerformanceTest(HashAlgorithm hashAlgorithm) {
      this.hashAlgorithm = hashAlgorithm;
   }

   @Test
   public void testHashDuration() {
      byte[] payload = payload("0123456789");

      long minDurationMilliSeconds = Long.MAX_VALUE;
      long maxDurationMilliSeconds = 0;
      long totalDurationMilliSeconds = 0;

      for (int i = 0; i < PERFORMANCE_TEST_ITERATIONS; i++) {
         StopWatch iterationChrono = StopWatch.createStarted();
         hashAlgorithm.hash(payload);
         iterationChrono.stop();

         long durationMilliSeconds = iterationChrono.getTime(TimeUnit.MILLISECONDS);
         minDurationMilliSeconds = min(minDurationMilliSeconds, durationMilliSeconds);
         maxDurationMilliSeconds = max(maxDurationMilliSeconds, durationMilliSeconds);
         totalDurationMilliSeconds = sum(totalDurationMilliSeconds, durationMilliSeconds);
      }

      long meanDurationMilliSeconds = totalDurationMilliSeconds / PERFORMANCE_TEST_ITERATIONS;
      System.out.printf("%s (%s) hash duration : mean=%dms, min=%dms, max=%dms%n",
                        hashAlgorithm.getClass().getSimpleName(),
                        hashAlgorithm.algorithmName(),
                        meanDurationMilliSeconds,
                        minDurationMilliSeconds,
                        maxDurationMilliSeconds);
      assertThat(meanDurationMilliSeconds).isLessThan(MAX_HASH_DURATION);
   }

}
