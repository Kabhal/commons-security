/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.algorithm;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.junit.jupiter.api.Test;

class SaltTest {

   @Test
   public void testInstanceOfBytesWhenNominal() {
      Salt salt = Salt.ofBytes(new byte[] { 1, 2, 3, 4 });

      assertThat(salt).isNotNull();
      assertThat(salt.asBytes()).isEqualTo(new byte[] { 1, 2, 3, 4 });
   }

   @Test
   public void testInstanceOfBytesWhenEmpty() {
      assertThatNullPointerException().isThrownBy(() -> Salt.ofBytes(null)).withMessage("'salt' must not be null");
      assertThatIllegalArgumentException().isThrownBy(() -> Salt.ofBytes(new byte[] {})).withMessage("'salt' must not be empty");
   }

   @Test
   public void testInstanceOfStringWhenNominal() {
      Salt salt = Salt.ofString("abc");

      assertThat(salt).isNotNull();
      assertThat(salt.asBytes()).isEqualTo(new byte[] { 97, 98, 99 });
   }

   @Test
   public void testInstanceOfStringWhenEmpty() {
      assertThatNullPointerException().isThrownBy(() -> Salt.ofString(null)).withMessage("'salt' must not be null");
      assertThatIllegalArgumentException().isThrownBy(() -> Salt.ofString("")).withMessage("'salt' must not be empty");
   }

   @Test
   public void testInstanceOfStringCharsetWhenNominal() {
      Salt salt = Salt.ofString("abc", StandardCharsets.ISO_8859_1);

      assertThat(salt).isNotNull();
      assertThat(salt.asBytes()).isEqualTo(new byte[] { 97, 98, 99 });
   }

   @Test
   public void testInstanceOfStringCharsetWhenNull() {
      assertThatNullPointerException().isThrownBy(() -> Salt.ofString("abc", null)).withMessage("'charset' must not be null");
   }

   @Test
   public void testInstancePseudoRandomWhenNominal() {
      Salt salt = Salt.pseudoRandom(4);

      assertThat(salt).isNotNull();
      assertThat(salt.asBytes()).hasSize(4);
   }

   @Test
   public void testInstancePseudoRandomWhenEmpty() {
      assertThatIllegalArgumentException().isThrownBy(() -> Salt.pseudoRandom(-1)).withMessage("salt length must be > 0");
   }

   @Test
   public void testInstancePseudoRandomWhenCustomRandom() throws NoSuchAlgorithmException {
      Salt salt = Salt.pseudoRandom(SecureRandom.getInstance("SHA1PRNG"), 4);

      assertThat(salt).isNotNull();
      assertThat(salt.asBytes()).hasSize(4);
   }

   @Test
   public void testInstancePseudoRandomWhenNullCustomRandom() {
      assertThatNullPointerException().isThrownBy(() -> Salt.pseudoRandom(null, 4)).withMessage("'randomGenerator' must not be null");
   }

}