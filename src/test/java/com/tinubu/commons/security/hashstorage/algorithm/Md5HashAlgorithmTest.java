/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.algorithm;

import static com.tinubu.commons.security.hashstorage.TestUtils.payload;
import static com.tinubu.commons.security.hashstorage.algorithm.Md5HashAlgorithm.Md5HashAlgorithmBuilder;
import static com.tinubu.commons.security.hashstorage.algorithm.Md5HashAlgorithm.Md5HashAlgorithmProvider;
import static java.util.Collections.emptyMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class Md5HashAlgorithmTest {

   @Nested
   public class Instance {

      @Test
      public void testInstanceWhenNominal() {
         Md5HashAlgorithm hashAlgorithm = new Md5HashAlgorithmBuilder().build();

         assertThat(hashAlgorithm.algorithmNames()).containsExactly("md5", "1");
         assertThat(hashAlgorithm.salt()).isEmpty();
         assertThat(hashAlgorithm.parameters()).isEmpty();
      }

      @Test
      public void testInstanceWhenSalt() {
         Md5HashAlgorithm hashAlgorithm =
               new Md5HashAlgorithmBuilder().salt(Salt.ofBytes(new byte[] { 84, -10, 22, 34 })).build();

         assertThat(hashAlgorithm.salt()).get().satisfies(salt -> assertThat(salt.asBytes()).hasSize(4));
         assertThat(hashAlgorithm.parameters()).isEmpty();
      }

      @Test
      public void testInstanceWhenNullSalt() {
         Md5HashAlgorithm hashAlgorithm = new Md5HashAlgorithmBuilder().salt(null).build();

         assertThat(hashAlgorithm.salt()).isEmpty();
         assertThat(hashAlgorithm.parameters()).isEmpty();
      }
   }

   @Nested
   public class Provider {

      @Test
      public void testProviderAlgorithmNames() {
         assertThat(new Md5HashAlgorithmProvider().algorithmNames()).containsExactlyInAnyOrder("md5", "1");
      }

      @Test
      public void testProviderFromFormatWhenNominal() {
         assertThat(new Md5HashAlgorithmProvider().fromFormat(Salt.ofString("salt"), emptyMap())).isNotNull();
      }

      @Test
      public void testProviderFromFormatWhenOptionalSalt() {
         assertThat(new Md5HashAlgorithmProvider().fromFormat(null, emptyMap())).isNotNull();
      }

      @Test
      public void testProviderFromFormatWhenBadParameters() {
         assertThatNullPointerException()
               .isThrownBy(() -> new Md5HashAlgorithmProvider().fromFormat(Salt.ofString("salt"), null))
               .withMessage("'parameters' must not be null");
      }

   }

   @Nested
   public class EqualityAndToString {

      @Test
      public void testEquals() {
         Md5HashAlgorithm hashAlgorithm1 = new Md5HashAlgorithmBuilder().build();
         Md5HashAlgorithm hashAlgorithm2 = new Md5HashAlgorithmBuilder().build();
         Md5HashAlgorithm hashAlgorithm3 = new Md5HashAlgorithmBuilder().salt(Salt.pseudoRandom(4)).build();

         assertThat(hashAlgorithm1).isEqualTo(hashAlgorithm2);
         assertThat(hashAlgorithm1).hasSameHashCodeAs(hashAlgorithm2);

         assertThat(hashAlgorithm1).isNotEqualTo(hashAlgorithm3);
      }

      @Test
      public void testToString() {
         assertThat(new Md5HashAlgorithmBuilder()
                          .salt(Salt.ofBytes(new byte[] { 84, -10, 22, 34 }))
                          .build()).hasToString("Md5HashAlgorithm[salt=Salt[salt=[84, -10, 22, 34]]]");
      }

   }

   @Nested
   public class Hash {

      @Test
      public void testHashWhenNominal() {
         Md5HashAlgorithm hashAlgorithm = new Md5HashAlgorithmBuilder().build();

         assertThat(hashAlgorithm.hash(payload("payload"))).isEqualTo(new byte[] {
               50, 28, 60, -12, -122, -19, 80, -111, 100, -19, -20, 30, 25, -127, -2, -56 });
      }

      @Test
      public void testHashWhenEmptyPayload() {
         Md5HashAlgorithm hashAlgorithm = new Md5HashAlgorithmBuilder().build();

         assertThat(hashAlgorithm.hash(payload(""))).isEqualTo(new byte[] {
               -44, 29, -116, -39, -113, 0, -78, 4, -23, -128, 9, -104, -20, -8, 66, 126 });
      }

      @Test
      public void testHashWhenNullPayload() {
         Md5HashAlgorithm hashAlgorithm = new Md5HashAlgorithmBuilder().build();

         assertThatNullPointerException().isThrownBy(() -> hashAlgorithm.hash(null)).withMessage("'payload' must not be null");
      }

   }

   @Nested
   public class ArchitecturalDesign {

      private static final int NB_ITERATIONS = 10;

      @Test
      public void testHashAlgorithmIsReusable() {
         List<Payload> expectedHashes = new ArrayList<>();
         IntStream
               .range(0, NB_ITERATIONS)
               .forEach(i -> expectedHashes.add(new Payload(new Md5HashAlgorithmBuilder()
                                                                  .build()
                                                                  .hash(("payload-" + i).getBytes(
                                                                        StandardCharsets.UTF_8)))));

         List<Payload> hashes = new ArrayList<>();
         Md5HashAlgorithm hashAlgorithm = new Md5HashAlgorithmBuilder().build();
         IntStream
               .range(0, NB_ITERATIONS)
               .forEach(i -> hashes.add(new Payload(hashAlgorithm.hash(("payload-" + i).getBytes(
                     StandardCharsets.UTF_8)))));

         assertThat(hashes).isEqualTo(expectedHashes);
      }

      class Payload {
         byte[] hash;

         public Payload(byte[] hash) {
            this.hash = hash;
         }

         @Override
         public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Payload payload = (Payload) o;
            return Arrays.equals(hash, payload.hash);
         }

         @Override
         public int hashCode() {
            return Arrays.hashCode(hash);
         }
      }

   }

   @Nested
   public class Performance extends PerformanceTest {

      public Performance() {
         super(new Md5HashAlgorithmBuilder().build());
      }

   }
}